import React from "react";
import Piece from "./Piece";

function Field({ onClick, x, y, piece, potentialMove, isActive }) {
  return (
    <div
      className={`field ${potentialMove ? "is-potential" : ""} ${
        isActive ? "is-active" : ""
      }`}
      onClick={() => onClick(x, y)}
    >
      {piece != null ? <Piece color={piece.color} type={piece.type} /> : null}
    </div>
  );
}

export default Field;
