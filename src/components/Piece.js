import React from "react";

function Piece({ color, type }) {
  return <div className={"piece " + color + " " + type} />;
}

export default Piece;
