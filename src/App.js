import React, { useState, useEffect } from "react";
import "./App.css";
import Field from "./components/Field";
import deepCopy from "./functions/deepCopy";
import moveSelectedPieceTo from "./functions/moveSelectedPieceTo";
import calculateGlobalTakePotential from "./functions/calculateGlobalTakePotential";
import calculatePotentialMoves from "./functions/calculatePotentialMoves";
import getPiece from "./functions/getPiece";
import promotePiece from "./functions/promotePiece";

let globalTakePotential = false;
let selectedPiece = false;

function Game() {
  const initialBoard = [
    [null, "x", null, "x", null, "x", null, "x", null, "x"],
    ["x", null, "x", null, "x", null, "x", null, "x", null],
    [null, "x", null, "x", null, "x", null, "x", null, "x"],
    ["x", null, "x", null, "x", null, "x", null, "x", null],
    [null, null, null, null, null, null, null, null, null, null],
    [null, null, null, null, null, null, null, null, null, null],
    [null, "o", null, "o", null, "o", null, "o", null, "o"],
    ["o", null, "o", null, "o", null, "o", null, "o", null],
    [null, "o", null, "o", null, "o", null, "o", null, "o"],
    ["o", null, "o", null, "o", null, "o", null, "o", null]
  ];

  const [blackPiecesCount, updateBlackPiecesCount] = useState(20);
  const [whitePiecesCount, updateWhitePiecesCount] = useState(20);
  const [moveNumber, updateMoveNumber] = useState(0);
  const [activePlayer, updateActivePlayer] = useState("white");
  const [history, updateHistory] = useState([
    { nextPlayer: "white", board: initialBoard }
  ]);
  const [potentialMoves, updatePotentialMoves] = useState([]);

  useEffect(() => {
    globalTakePotential = calculateGlobalTakePotential(
      history[moveNumber].board,
      activePlayer
    );
  }, [moveNumber]);

  function movePiece(x, y) {
    const newHistory = deepCopy(history.slice(0, moveNumber + 1));
    let newBoard = deepCopy(newHistory[moveNumber].board);
    let moved = false;
    let taken = false;

    for (let i = 0; i < potentialMoves.length; i++) {
      const move = potentialMoves[i];
      const moveX = move.pos[0];
      const moveY = move.pos[1];
      const take = move.take;

      if (x === moveX && y === moveY) {
        newBoard = moveSelectedPieceTo(x, y, newBoard, selectedPiece);
        moved = true;
        if (take) {
          takePiece(take[0], take[1], newBoard);
          taken = true;
        }
      }
    }

    globalTakePotential = calculateGlobalTakePotential(newBoard, activePlayer);
    if (taken && globalTakePotential) {
      selectedPiece = { x: x, y: y, props: getPiece(newBoard[y][x]) };
      updatePotentialMoves(
        calculatePotentialMoves(newBoard, selectedPiece, globalTakePotential)
      );
      updateHistory(
        newHistory.concat([
          {
            nextPlayer: activePlayer,
            board: newBoard
          }
        ])
      );
      updateMoveNumber(newHistory.length);
      return;
    }

    if (moved) {
      globalTakePotential = false;

      if (selectedPiece.props.color === "white" && y === 0) {
        newBoard = promotePiece(x, y, newBoard, "white");
      }

      if (selectedPiece.props.color === "black" && y === 9) {
        newBoard = promotePiece(x, y, newBoard, "black");
      }

      updateHistory(
        newHistory.concat([
          {
            nextPlayer: activePlayer === "white" ? "black" : "white",
            board: newBoard
          }
        ])
      );
      updateMoveNumber(newHistory.length);
      passTurn();
    }
  }

  useEffect(() => {
    if (blackPiecesCount === 0) {
      alert("White won!");
    }

    if (whitePiecesCount === 0) {
      alert("Black won!");
    }
  }, [blackPiecesCount, whitePiecesCount]);

  function takePiece(x, y, board) {
    board[y][x] = null;
    if (activePlayer === "white") {
      updateBlackPiecesCount(old => --old);
    } else {
      updateWhitePiecesCount(old => --old);
    }
  }

  function passTurn() {
    selectedPiece = false;
    updatePotentialMoves([]);
    updateActivePlayer(activePlayer === "white" ? "black" : "white");
  }

  function jumpTo(move) {
    selectedPiece = false;
    globalTakePotential = false;
    updatePotentialMoves([]);
    updateMoveNumber(move);
    updateActivePlayer(history[move].nextPlayer);
  }

  function resetGame() {
    selectedPiece = false;
    updateMoveNumber(0);
    updateBlackPiecesCount(20);
    updateWhitePiecesCount(20);
    updateActivePlayer("white");
    updateHistory([{ nextPlayer: "white", board: initialBoard }]);
    updatePotentialMoves([]);
  }

  function clickHandler(x, y) {
    const board = history[moveNumber].board;
    const clickedPiece = getPiece(board[y][x]);

    if (board[y][x] != null && activePlayer === clickedPiece.color) {
      selectedPiece = { x: x, y: y, props: clickedPiece };
      updatePotentialMoves(
        calculatePotentialMoves(board, selectedPiece, globalTakePotential)
      );
    } else if (selectedPiece) {
      movePiece(x, y);
    }
  }

  function renderField(x, y, piece, potentialMove, isActive) {
    return (
      <Field
        key={"field" + (x + y * 6)}
        piece={piece}
        x={x}
        y={y}
        potentialMove={potentialMove}
        isActive={isActive}
        onClick={(x, y) => clickHandler(x, y)}
      />
    );
  }

  const currentBoard = history[moveNumber].board;

  let fields = [];
  for (let y = 0; y < currentBoard.length; y++) {
    let row = [];
    for (let x = 0; x < currentBoard[y].length; x++) {
      let potentialMove = false;
      let isActive = false;

      potentialMoves.forEach(move => {
        if (move.pos[0] === x && move.pos[1] === y) {
          potentialMove = true;
        }
      });

      if (selectedPiece && selectedPiece.x === x && selectedPiece.y === y) {
        isActive = true;
      }

      const field = renderField(
        x,
        y,
        getPiece(currentBoard[y][x]),
        potentialMove,
        isActive
      );
      row.push(field);
    }
    fields.push(
      <div key={"row" + y} className="row">
        {row}
      </div>
    );
  }

  return (
    <div className="game">
      <div className="board">{fields}</div>
      <div className="info">
        Current player: {activePlayer}. Current move: {moveNumber + 1}
      </div>
      <div className="history">
        {moveNumber > 0 ? (
          <button onClick={() => jumpTo(moveNumber - 1)}>Previous move</button>
        ) : null}
        {moveNumber < history.length - 1 ? (
          <button onClick={() => jumpTo(moveNumber + 1)}>Next move</button>
        ) : null}
        <button onClick={() => resetGame()}>Reset game</button>
      </div>
    </div>
  );
}

export default Game;
