function getPiece(char) {
  const pieces = {
    o: {
      color: "white",
      type: "normal"
    },
    oo: {
      color: "white",
      type: "king"
    },
    x: {
      color: "black",
      type: "normal"
    },
    xx: {
      color: "black",
      type: "king"
    },
    null: null
  };

  return pieces[char];
}

export default getPiece;
