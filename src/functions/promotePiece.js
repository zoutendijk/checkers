import deepCopy from "./deepCopy";
function promotePiece(x, y, board, color) {
  const newBoard = deepCopy(board);

  newBoard[y][x] = color === "white" ? "oo" : "xx";
  return newBoard;
}

export default promotePiece;
