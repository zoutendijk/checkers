import getPiece from "./getPiece";

function calculatePotentialMoves(board, selectedPiece, globalTakePotential) {
  if (!selectedPiece) return;

  const takePositions = [[1, 1], [-1, 1], [-1, -1], [1, -1]];
  const currentX = selectedPiece.x;
  const currentY = selectedPiece.y;
  const activePiece = selectedPiece.props;
  let moves = [];

  // Normal piece
  if (activePiece.type === "normal") {
    // Take potential
    for (let i = 0; i < takePositions.length; i++) {
      let pos = takePositions[i];
      if (
        currentY + pos[0] < 10 &&
        currentY + pos[0] > 0 &&
        currentX + pos[1] < 10 &&
        currentX + pos[1] > 0 &&
        board[currentY + pos[0]][currentX + pos[1]] != null &&
        getPiece(board[currentY + pos[0]][currentX + pos[1]]).color !==
          activePiece.color &&
        board[currentY + pos[0] * 2][currentX + pos[1] * 2] === null
      ) {
        moves.push({
          pos: [currentX + pos[1] * 2, currentY + pos[0] * 2],
          take: [currentX + pos[1], currentY + pos[0]]
        });
      }
    }

    // Move potential (only check if there is no take potential)
    if (moves.length === 0 && !globalTakePotential) {
      if (activePiece.color === "white") {
        if (board[currentY - 1][currentX - 1] === null) {
          moves.push({ pos: [currentX - 1, currentY - 1], take: false });
        }

        if (board[currentY - 1][currentX + 1] === null) {
          moves.push({ pos: [currentX + 1, currentY - 1], take: false });
        }
      }

      if (activePiece.color === "black") {
        if (board[currentY + 1][currentX - 1] === null) {
          moves.push({ pos: [currentX - 1, currentY + 1], take: false });
        }

        if (board[currentY + 1][currentX + 1] === null) {
          moves.push({ pos: [currentX + 1, currentY + 1], take: false });
        }
      }
    }
  }

  // King
  if (activePiece.type === "king") {
    let checkedDirections = 0;
    let directions = [[1, 1], [-1, 1], [-1, -1], [1, -1]];
    let opponentPieces = [];
    let takePotential = false;
    let currentFieldX;
    let currentFieldY;

    function filterTakeMoves() {
      moves = moves.filter(move => {
        return move.take;
      });
    }

    function changeDirection() {
      checkedDirections++;
      currentFieldX = currentX;
      currentFieldY = currentY;
      opponentPieces = [];
    }

    function checkDiagonalSquare(currentField) {
      currentFieldX = currentField[0];
      currentFieldY = currentField[1];

      if (
        currentFieldX > 9 ||
        currentFieldX < 0 ||
        currentFieldY > 9 ||
        currentFieldY < 0
      ) {
        changeDirection();
      } else {
        const field = board[currentFieldY][currentFieldX];
        if (field === null) {
          if (opponentPieces.length === 1) {
            moves.push({
              pos: [currentFieldX, currentFieldY],
              take: opponentPieces[0]
            });
            if (!takePotential) filterTakeMoves();
            takePotential = true;
          } else if (!takePotential && !globalTakePotential) {
            moves.push({ pos: [currentFieldX, currentFieldY], take: false });
          }
        } else if (getPiece(field).color === activePiece.color) {
          changeDirection();
        } else if (getPiece(field).color !== activePiece.color) {
          if (opponentPieces.length > 0) {
            changeDirection();
          } else {
            opponentPieces.push([currentFieldX, currentFieldY]);
          }
        }
      }

      if (checkedDirections === 4) {
        return;
      }

      checkDiagonalSquare([
        currentFieldX + directions[checkedDirections][0],
        currentFieldY + directions[checkedDirections][1]
      ]);
    }

    checkDiagonalSquare([
      currentX + directions[checkedDirections][0],
      currentY + directions[checkedDirections][1]
    ]);
  }

  return moves;
}

export default calculatePotentialMoves;
