import deepCopy from "./deepCopy";

const moveSelectedPieceTo = (x, y, board, selectedPiece) => {
  const currentX = selectedPiece.x;
  const currentY = selectedPiece.y;
  const piece = board[currentY][currentX];
  const newBoard = deepCopy(board);

  newBoard[currentY][currentX] = null;
  newBoard[y][x] = piece;
  return newBoard;
};

export default moveSelectedPieceTo;
