import getPiece from "./getPiece";

function calculateGlobalTakePotential(board, activePlayer) {
  const takePositions = [[1, 1], [-1, 1], [-1, -1], [1, -1]];

  for (let y = 0; y < board.length; y++) {
    const row = board[y];

    for (let x = 0; x < row.length; x++) {
      const piece = getPiece(board[y][x]);
      if (piece !== null && piece.color === activePlayer) {
        if (piece.type === "normal") {
          for (let i = 0; i < takePositions.length; i++) {
            let pos = takePositions[i];

            if (
              y + pos[0] < 10 &&
              y + pos[0] > 0 &&
              x + pos[1] < 10 &&
              x + pos[1] > 0 &&
              board[y + pos[0]][x + pos[1]] != null &&
              getPiece(board[y + pos[0]][x + pos[1]]).color !== activePlayer &&
              board[y + pos[0] * 2][x + pos[1] * 2] === null
            ) {
              return true;
            }
          }
        }
        if (piece.type === "king") {
          let checkedDirections = 0;
          let directions = [[1, 1], [-1, 1], [-1, -1], [1, -1]];
          let opponentPieces = [];
          let currentFieldX;
          let currentFieldY;

          function changeDirection() {
            checkedDirections++;
            currentFieldX = x;
            currentFieldY = y;
            opponentPieces = [];
          }

          function checkDiagonalSquare(currentField) {
            currentFieldX = currentField[0];
            currentFieldY = currentField[1];

            if (
              currentFieldX > 9 ||
              currentFieldX < 0 ||
              currentFieldY > 9 ||
              currentFieldY < 0
            ) {
              changeDirection();
            } else {
              const field = board[currentFieldY][currentFieldX];
              if (field === null) {
                if (opponentPieces.length === 1) {
                  return true;
                }
              } else if (getPiece(field).color === piece.color) {
                changeDirection();
              } else if (getPiece(field).color !== piece.color) {
                if (opponentPieces.length > 0) {
                  changeDirection();
                } else {
                  opponentPieces.push([currentFieldX, currentFieldY]);
                }
              }
            }

            if (checkedDirections === 4) {
              return;
            }

            return checkDiagonalSquare([
              currentFieldX + directions[checkedDirections][0],
              currentFieldY + directions[checkedDirections][1]
            ]);
          }

          return checkDiagonalSquare([
            x + directions[checkedDirections][0],
            y + directions[checkedDirections][1]
          ]);
        }
      }
    }
  }
}

export default calculateGlobalTakePotential;
